class Point:

    time = 0.0
    flux = 0.0

    def __init__(self, time, flux):
        self.time = time
        self.flux = flux

    def __str__(self):
        return "Point(%.2f, %.2f)" % (self.time, self.flux)

    def serialize(self):
        return {
            'time': self.time,
            'flux': self.flux,
        }

import random
from math import sin, pi

def gen_point():
    if 'gen_time' not in globals():
        gen_time = 0.0
        global gen_time
    time = gen_time
    gen_time = gen_time + 1
    period = 40.0
    flux = 30 * sin((time / period ) *  2 * pi) + random.gauss(100, 10)
    return Point(time, flux)
